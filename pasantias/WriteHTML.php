<?php
require('fpdf.php');

class PDF_HTML extends FPDF
{
	var $B=0;
	var $I=0;
	var $U=0;
	var $HREF='';
	var $ALIGN='';
	private $day;
    private $month;
    private $year;
    private $monthArray = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];   

	function WriteHTML($html)
	{
		//HTML parser
		$html=str_replace("\n",' ',$html);
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				//Text
				if($this->HREF)
					$this->PutLink($this->HREF,$e);
				elseif($this->ALIGN=='center')
					$this->Cell(0,5,$e,0,1,'C');
				else
					$this->Write(5,$e);
			}
			else
			{
				//Tag
				if($e[0]=='/')
					$this->CloseTag(strtoupper(substr($e,1)));
				else
				{
					//Extract properties
					$a2=explode(' ',$e);
					$tag=strtoupper(array_shift($a2));
					$prop=array();
					foreach($a2 as $v)
					{
						if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
							$prop[strtoupper($a3[1])]=$a3[2];
					}
					$this->OpenTag($tag,$prop);
				}
			}
		}
	}

	function OpenTag($tag,$prop)
	{
		//Opening tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,true);
		if($tag=='A')
			$this->HREF=$prop['HREF'];
		if($tag=='BR')
			$this->Ln(5);
		if($tag=='P')
			$this->ALIGN=$prop['ALIGN'];
		if($tag=='HR')
		{
			if( !empty($prop['WIDTH']) )
				$Width = $prop['WIDTH'];
			else
				$Width = $this->w - $this->lMargin-$this->rMargin;
			$this->Ln(2);
			$x = $this->GetX();
			$y = $this->GetY();
			$this->SetLineWidth(0.4);
			$this->Line($x,$y,$x+$Width,$y);
			$this->SetLineWidth(0.2);
			$this->Ln(2);
		}
	}

	function CloseTag($tag)
	{
		//Closing tag
		if($tag=='B' || $tag=='I' || $tag=='U')
			$this->SetStyle($tag,false);
		if($tag=='A')
			$this->HREF='';
		if($tag=='P')
			$this->ALIGN='';
	}

	function SetStyle($tag,$enable)
	{
		//Modify style and select corresponding font
		$this->$tag+=($enable ? 1 : -1);
		$style='';
		foreach(array('B','I','U') as $s)
			if($this->$s>0)
				$style.=$s;
		$this->SetFont('',$style);
	}

	function PutLink($URL,$txt)
	{
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}
	function Header(){
		// Calculate width of title and position

        // Calculate width of title and position
        $this->Image('../IMAGENES/logouru.png',92,0,25,20,'PNG');
        $this->Ln(10);
        // Title
        $this->SetFont('Arial','B',12);
        $this->Cell(0,9,'Universidad Rafael Urdaneta',0,1,'C',false);
        $this->SetFont('Arial','B',5);
        $this->Cell(0,5,'Universidad Sin Fines De Lucro',0,1,'C',false);
        // Line break
        $this->Ln(40);
	}
	function Footer(){
        // Position at 2 cm from bottom
        $this->SetY(-41);
        // Arial italic 8
        $this->SetFont('Arial','I',9);
        // Text color in gray
        $this->SetTextColor(128);
        // Page number
        $this->line($this->GetX(),$this->GetY(),$this->GetX()*20,$this->GetY());
        $this->MultiCell(0,10,'NUEVA SEDE VEREDA DEL LAGO Entrada sur del paseo del lago, Avda 2.(El Milagro) con calle 86(La Calzada) Maracaibo, Venezuela. Tlfonos (58)(261)2000-URU(878)');

    
	}
	function ChapterTitle($titleArray){
        $thisMonth = date('m');
        $this->day =  date('d');
        $this->month = $this->monthArray[intval($thisMonth)-1];
        $this->year =  date('Y');
        $this->SetFont('Arial','',12);
        // Title
        $this->Cell(0,6,"Maracaibo, $this->day de $this->month del $this->year",0,4,'R',false);
        // Line break
        $this->Ln(2);
        list($title,$name,$role,$place) = $titleArray;
        $this->SetFont('Arial','B',12);
        $this->Cell(0,5,utf8_decode("$title. $name"),0,0,'L',false);
        $this->Ln(4);
        $this->Cell(0,5,utf8_decode("$role"),0,0,'L',false);
        $this->Ln(4);
        $this->Cell(0,5,utf8_decode("$place"),0,0,'L',false);
        $this->ln(10);
        $this->SetFont('Arial','',12);

    }
    function Signature($professorArray){
    	$this->SetY(-105);
         $this->SetFont('Arial','',12);
        // Output justified text
        $this->MultiCell(0,5,utf8_decode("Agradeciendo la mayor colaboración que les puedan brindar a este estudiante, se suscribe a usted."));
        $this->Ln(10);
        $this->Cell(0,5,utf8_decode("Atentamente.-"),0,0,'C',false);
         // Line break
        $this->Ln(30);
        list($professorName,$professorSchool) = $professorArray; 
        $this->SetFont('Times','B',12);
        $this->Cell(0,5,utf8_decode($professorName),0,0,'C',false);
        $this->Ln(4);
        $this->Cell(0,5,utf8_decode($professorSchool),0,0,'C',false);
    }
}
?>
