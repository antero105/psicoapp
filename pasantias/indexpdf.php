<?php
require_once'WriteHTML.php';
function getTittle(){
	$personRole = ucwords($_POST['destinatario']);
	$personName = ucwords($_POST['nombreDestinatario']);
	$personJob = ucwords($_POST['cargo']);
	$personPlace = ucwords($_POST['lugar']);
	return array($personRole,$personName,$personJob,$personPlace);

}
function getBody(){
	$name = ucfirst($_POST['nombre']);
	$year = date('Y');
	$lastName = ucfirst($_POST['apellido']);
	$id = number_format($_POST['cedula'], 0, '', '.');
	$school = $_POST['pasantia'];
	$period = $_POST['periodo'];
	switch ($school) {
		case 's1':
			$pasanName = 'Psicología Social I';
			$schoolName = 'Psicólogo(a)';
			break;
		case 's2':
			$pasanName = 'Psciología Social II';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'c1': 
			$pasanName = 'Pscicología Clínica I';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'c2':
			$pasanName = 'Psicología Clínica II';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'e1':
			$pasanName = 'Psicología Escolar I';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'e2':
			$pasanName = 'Psicología Escolar II';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'o1':
			$pasanName = 'Psicología Organizacional I';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'o2':
			$pasanName = 'Psicología Organizacional II';
			$schoolName = 'Psicólogo(a)';
			break;
		case 'ingE':
			$pasanName = 'Ingeniería Eléctrica';
			$schoolName = 'Ingeniero(a) Eléctrico';
			break;
		case 'ingC':
			$pasanName = 'Ingeniería en Computación';
			$schoolName = 'Ingeniero(a) en Computación';
			break;
		case 'ingCx':
			$pasanName = 'Ingeniería Civil';
			$schoolName = 'Ingeniero(a) Civil';
			break;
		case 'ingI':
			$pasanName = 'Ingeniería Industrial';
			$schoolName = 'Ingeniero(a) Industrial';

			break;
		case 'ingT':
			$pasanName = 'Ingeniería de Telecomunicaciones';
			$schoolName = 'Ingeniero(a) de Telecomunicaciones';
			break;
		default:
			# code...
			break;
		}
		switch ($period) {
			case 'A':
				$periodName ='Enero-Abril';
				break;
			case 'B':
				$periodName = 'Junio-Septiembre';
				break;
			case 'C':
				$periodName = 'Septiembre-Enero';
				break;

			default:
				# code...
				break;
		}

		
		$response = utf8_decode("<p align=justify>En grado de dirigirnos a usted(es), en la ocasión de solicitar que sea considerada la posilibidad de realización de pasantía para el(la) <b>Br. $name $lastName</b>, titular de la cédula de identidad Nro <b>$id</b> estudiante de nuestra institución.</p>
			<br><br>
			<p align=justify>La realización de pasantía para <b>$pasanName</b> constituye un requisito indispensable para aspirar al título de <b>$schoolName</b>, dicha pasantía deberán cumplirse en el periodo <b>$periodName</b> del año $year, y en un lapso no mayor de <b>240 HORAS</b>.</p>");
		return $response;
}
function getSignature(){
	$school = $_POST['pasantia'];
	switch ($school) {
		case 's1':
			return array('Psic. Luis Landaeta','Escuela De Psicología');
			break;
		case 's2':
			return array('Psic. Luis Landaeta','Escuela De Psicología');
			break;
		case 'c1': 
			return array('Psic. Angela Rotuno','Escuela De Psicología');
			break;
		case 'c2':
			return array('Psic. Angela Rotuno','Escuela De Psicología');
			break;
		case 'e1':
			return array('Psic. Elvia Barboza','Escuela De Psicología');
			break;
		case 'e2':
			return array('Psic. Elvia Barboza','Escuela De Psicología');
			break;
		case 'o1':
			return array('Psic. Zorida','Escuela De Psicología');
			break;
		case 'o2':
			return array('Psic. Zorida','Escuela De Psicología');
			break;
		case 'ingE':
			return array('Ing. Arnaldo Largo','Escuela De Ingeniería Eléctrica');
			break;
		case 'ingC':
			return array('Ing. Jubert Pérez Zabala','Escuela de Ingeniería en Computación');
			break;
		case 'ingCx':
			return array('Ing. Nancy Urdaneta','Escuela de Ingeniería Civil');
			break;
		case 'ingI':
			return array('Ing. Ana Irene Rivas','Escuela de Ingeniería Industrial');
			break;
		case 'ingT':
			return array('Ing. Carlos Belinskif Serwachjk','Escuela de Ingeniería de Telecomunicaciones');
			break;
		default:
			# code...
			break;
	}

}
function genPdf(){
	$pdf=new PDF_HTML();	
	$pdf->AddPage('P','Letter');
	$pdf->SetAuthor('Universidad Rafael Urdaneta');
	$pdf->SetFont('Arial');
	$pdf->ChapterTitle(getTittle());
	$pdf->SetFontSize(12);
	$pdf->WriteHTML(getBody());
	$pdf->Signature(getSignature());
	$pdf->Footer();
	$pdf->Output('Solicitud De Pasantia','I');

}


genPdf();	
?>