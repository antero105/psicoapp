<?php
	require_once 'WriteHTML.php';
function getTittle(){
	$personRole = ucwords($_POST['destinatario']);
	$personName = ucwords($_POST['nombreDestinatario']);
	$personJob = ucwords($_POST['cargo']);
	$personPlace = ucwords($_POST['lugar']);
	return array($personRole,$personName,$personJob,$personPlace);

}
function getBody(){


	if(isset($_POST['nombre2']) && isset($_POST['apellido2']) && isset($_POST['cedula2'])){
		$name = ucfirst($_POST['nombre']);
		$lastName = ucfirst($_POST['apellido']);
		$id = number_format($_POST['cedula'], 0, '', '.');
		$name2 = ucfirst($_POST['nombre2']);
		$lastName2 = ucfirst($_POST['apellido2']);
		$id2 = number_format($_POST['cedula2'], 0, '', '.');
		$tuthorName = ucfirst($_POST['nombreT']);
		$tuthorTittle = ucfirst($_POST['tituloT']);
		$tesisName = ucfirst($_POST['nombreI']);
		$period = $_POST['periodo'];
		switch ($period) {
			case 'A':
				$periodName ='Enero-Abril';
				break;
			case 'B':
				$periodName = 'Junio-Septiembre';
				break;
			case 'C':
				$periodName = 'Septiembre-Enero';
				break;

			default:
				# code...
				break;
		}

		$response = utf8_decode("<p align=justify> Reciba un cordial saludo de la escuela de Psicología. <br><br></p>
			<p align=justify>La presente tiene como fin solicitarle su autorización para que le permita a los(as) bachilleres <b>$name $lastName,</b> titular de la cédula de identidad Nº <b>$id,</b> y <b>$name2 $lastName2</b> titular de la cédula de identidad Nº <b>$id2,</b> a ingresar a las instalaciones para aplicar el instrumento, con el fin de realizar su Trabajo Especial de Grado, titulado <b>\"$tesisName\"</b>.En el periodo <b>$periodName</b>.La misma es tutoreada por el(la) <b>$tuthorTittle. $tuthorName</b></p>");
		return $response;

	}
	else{
		$name = ucfirst($_POST['nombre']);
		$lastName = ucfirst($_POST['apellido']);
		$id = number_format($_POST['cedula'], 0, '', '.');
		$tuthorName = ucwords($_POST['nombreT']);
		$tuthorTittle = ucfirst($_POST['tituloT']);
		$tesisName = ucwords($_POST['nombreI']);
		$period = $_POST['periodo'];
		switch ($period) {
			case 'A':
				$periodName ='Enero-Abril';
				break;
			case 'B':
				$periodName = 'Junio-Septiembre';
				break;
			case 'C':
				$periodName = 'Septiembre-Enero';
				break;

			default:
				# code...
				break;
		}

		$response = utf8_decode("<p align=justify> Reciba un cordial saludo de la escuela de Psicología. <br><br></p>
			<p align=justify>La presente tiene como fin solicitarle su autorización para que le permita al bachiller <b>$name $lastName,</b> titular de la cédula de identidad Nº <b>$id</b>, ingresar a las instalaciones para aplicar el instrumento, con el fin de realizar su Trabajo Especial de Grado, titulado <b>\"$tesisName\"</b>.En el periodo <b>$periodName</b>.La misma es tutoreada por el(la) <b>$tuthorTittle. $tuthorName</b></p>");
		return $response;
	}
}
function getSignature(){
	return array('Psic. Gilberto Zuleta','Director de la Escuela de Psicologia');
}
function genPdf(){
		$pdf=new PDF_HTML();	
		$pdf->AddPage('P','Letter');
		$pdf->SetAuthor('Universidad Rafael Urdaneta');
		$pdf->SetFont('Arial');
		$pdf->ChapterTitle(getTittle());
		$pdf->SetFontSize(12);
		$pdf->WriteHTML(getBody());
		$pdf->Signature(getSignature());
		$pdf->Footer();
		$pdf->Output('Aplicacion de Instrumento','I');
		
	}
genPdf();
?>
